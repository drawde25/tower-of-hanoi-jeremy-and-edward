let tower1 = document.querySelector("#tower1")
let tower2 = document.querySelector("#tower2")
let tower3 = document.querySelector("#tower3")
let selectedItem 
let isSelected = false


tower1.addEventListener("click", firstClick)
tower2.addEventListener("click", firstClick)
tower3.addEventListener("click", firstClick)

tower1.addEventListener("click", secondClick)
tower2.addEventListener("click", secondClick)
tower3.addEventListener("click", secondClick)

function firstClick() {
    selectedItem = event.currentTarget.lastElementChild 
    isSelected = true
}

function secondClick () {
    let topBlock = event.currentTarget.lastElementChild
    if(isSelected){
        if(!topBlock) {
            event.currentTarget.appendChild(selectedItem)
            isSelected = false
            selectedItem = undefined
        }
        else if(selectedItem.clientWidth < topBlock.clientWidth){
            event.currentTarget.appendChild(selectedItem)
            isSelected = false
            selectedItem = undefined

    
        }
    }

}

